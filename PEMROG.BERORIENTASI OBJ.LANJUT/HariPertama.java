/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mypkg;

/**
 *
 * @author lik-305--34
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        tugas();
    }

    static void pertama() {
        System.out.println("Nama        : Yoga Setiawan");
        System.out.println("Jurusan     : Informatika");
        System.out.println("Fakultas    : Ilmu Komputer");
        System.out.println("Angkatan    : 2014");
    }

    static void kedua() {
        System.out.println("Luas Bujur Sangkar ");
        int s1, s2, luas;
        s1 = 5;
        s2 = 5;
        luas = s1 * s2;
        System.out.println("Sisi I  = " + s1);
        System.out.println("Sisi II = " + s2);
        System.out.println("Luas    = " + luas);
    }

    static void ketiga() {
        System.out.println("Soal Ke 1");
        double total, dp, sisa, harga;
        int jumlah;

        jumlah = 20;
        harga = 5000;

        total = jumlah * harga;
        dp = total * 0.10;

        sisa = total - dp;
        System.out.println("Pak doni membeli kayu dengan jumlah " + jumlah + " Kubik");
        System.out.println("Dengan Harga perkubik yaitu " + harga);
        System.out.println("Pak doni membeli kayu dengan total pembayaran = " + total);
        System.out.println("Pak doni harus membayar sebesar = " + dp + " Sebagai DP");
        System.out.println("Dan sisa pembayaran nya sebesar " + sisa + " akan di bayar bila barang samapai");
    }
    static void tugas(){
        /**
         SOAL
         Sultan akan membayar PAM
         Pemakaian bulan lalu ...Kubik
         Sampai saat ini ...Kubik
         Ia mendapatkan potongan rp 500/kubik dari pembayaranya
        */

        int jml_bln_lalu, jml_bln_ini, total;
        double harga, potongan;

        jml_bln_ini = 20;
        jml_bln_lalu = 10;
        harga = 5000;

        total = jml_bln_ini + jml_bln_lalu;
        potongan = total * 500;
        harga = (total * harga) - potongan;

        System.out.println("Sultan harus membayar PAM dengan rincian sebagai berikut :");
        System.out.println("Pemakaian bulan lalu yaitu : "+jml_bln_lalu+" Kubik");
        System.out.println("Pemakaian bulan ini yaitu : "+jml_bln_ini+" Kubik");
        System.out.println("Dengan total pemakaian yaitu : Rp. "+total+ " Kubik");
        System.out.println("dan sultan mendapatkan potogan sebesar : Rp. "+potongan);
        System.out.println("Jadi sultan hanya harus membayar : Rp. "+harga);
    }
}
